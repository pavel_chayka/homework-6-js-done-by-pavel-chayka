const elements = ['hello', 'world', 23, '23', null];
function filterBy(arr, typeOfElement) {
    return arr.filter(element => typeof element !== typeOfElement);
}
console.log(filterBy(['hello', 'world', 23, '23', null],"string"));

